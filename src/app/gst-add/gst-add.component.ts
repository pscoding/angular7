import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { CompanyService } from '../company.service';
import Company from '../Company';



@Component({
  selector: 'app-gst-add',
  templateUrl: './gst-add.component.html',
  styleUrls: ['./gst-add.component.css']
})
export class GstAddComponent implements OnInit {
  employees: Company[];
  angForm: FormGroup;
  constructor(private fb: FormBuilder, private cs: CompanyService, private route: ActivatedRoute,
    private router: Router) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
      person_name: ['', Validators.required ],
      person_surname: ['', Validators.required ],
      person_address: ['', Validators.required ],
      person_postal_number: ['', Validators.required ],
      person_city: ['', Validators.required ]
    });
  }

  addEmployee(person_name, person_surname, person_address, person_postal_number, person_city) {
    this.cs.addEmployee(person_name, person_surname, person_address, person_postal_number, person_city);
    this.cs.getEmployees().subscribe((data: Company[]) => {
        this.employees = data;
        this.router.navigate(['/company']);

      });
  }

  ngOnInit() {
    this.cs.getEmployees();
  }

}