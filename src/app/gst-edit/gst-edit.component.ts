import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { Message } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { CompanyService } from '../company.service';
import Company from '../Company';


@Component({
  selector: 'app-gst-edit',
  templateUrl: './gst-edit.component.html',
  styleUrls: ['./gst-edit.component.css'],
  providers: [MessageService]

})
export class GstEditComponent implements OnInit {
  employee: any = {};
  angForm: FormGroup;
  display: boolean;
  msgs: Message[] = [];


  constructor(private route: ActivatedRoute,
    private router: Router,
    private cs: CompanyService,
    private fb: FormBuilder, private messageService: MessageService) {
      this.createForm();
 }

  createForm() {
    this.angForm = this.fb.group({
        person_name: ['', Validators.required ],
        person_surname: ['', Validators.required ],
        person_address: ['', Validators.required ],
        person_postal_number: ['', Validators.required ],
        person_city: ['', Validators.required ]
      });
    }

  ngOnInit() {
    this.route.params.subscribe(params => {
        this.cs.editSelectedEmployee(params['id']).subscribe(res => {
          this.employee = res;
      });
    });
  }

  updateEmployee(person_name, person_surname, person_address,person_postal_number,person_city){
      this.route.params.subscribe(params => {
      this.cs.updateEmployee(person_name, person_surname, person_address,person_postal_number,person_city, params['id']).subscribe((data: string) =>{
      console.log(data);
      this.addUpdateNotification();
    });
    })
  }

  showDeleteDialog() {
    if(!this.display) {
      this.display = true;
      console.log('Clicked delete button...Dialog is visible');
    } else {
      console.log('Delete dialog is already visible!');
    }
  }

  deleteEmployee(id) {
    this.cs.deleteEmployee(id).subscribe(res => {
      this.cs.getEmployees().subscribe((data: Company[]) => {
        this.employee = data;
      });
      this.display = false;
      console.log('Deleted');
      console.log('ID of Employee: ' + id);
      this.addDeleteNotification();
    });
  }

  addDeleteNotification() {
    this.messageService.add({severity:'success', summary:'Employee deleted!', detail:'<a href="/company">Click here if you want to check other employees</a>'});
  }

  addUpdateNotification() {
    this.messageService.add({severity:'success', summary:'Employee updated!', detail:'<a href="/company">Click here if you want to check other employees</a>'});
  }
}