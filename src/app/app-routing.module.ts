import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GstAddComponent } from './gst-add/gst-add.component';
import { GstEditComponent } from './gst-edit/gst-edit.component';
import { GstGetComponent } from './gst-get/gst-get.component';
import { PageComponent } from './page/page.component';


const routes: Routes = [
  {
    path: 'employee/create',
    component: GstAddComponent
  },
  {
    path: 'edit/:id',
    component: GstEditComponent,
    data: { title: 'Edit Product' }
  },
  {
    path: 'company',
    component: GstGetComponent
  },
  {
    path: 'view/:id',
    component: PageComponent,
    data: { title: 'View Employee' }
  },
  {
    path: '',
    component: GstAddComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
