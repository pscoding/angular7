import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {
  uri = 'http://localhost:4000/company';

  constructor(private http: HttpClient) { }

  addEmployee(person_name, person_surname, person_address, person_postal_number, person_city) {
    const obj = {
      person_name: person_name,
      person_surname: person_surname,
      person_address: person_address,
      person_postal_number: person_postal_number,
      person_city: person_city
    };
    console.log(obj);
    return this.http.post(`${this.uri}/add`, obj)
        .subscribe(res => console.log('Done'));
  }

  getEmployees() {
    return this
           .http
           .get(`${this.uri}`);
  }

  deleteEmployee(id) {
    return this
              .http
              .get(`${this.uri}/delete/${id}`);
  }

  editSelectedEmployee(id) {
    return this.http.get(`${this.uri}/edit/${id}`);
  }
  

  public updateEmployee(person_name, person_surname, person_address, person_postal_number, person_city, id) {
    const obj = {
        person_name: person_name,
        person_surname: person_surname,
        person_address: person_address,
        person_postal_number: person_postal_number,
        person_city: person_city

      };
    return this.http.post(`${this.uri}/update/${id}`, obj);
  }
}
