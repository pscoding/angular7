import { Component, OnInit } from '@angular/core';
import Company from '../Company';
import { CompanyService } from '../company.service';

@Component({
  selector: 'app-gst-get',
  templateUrl: './gst-get.component.html',
  styleUrls: ['./gst-get.component.css']
})
export class GstGetComponent implements OnInit {

  employees: Company[];
  cols: any[];


  constructor(private bs: CompanyService) { }

  ngOnInit() {
    this.bs
      .getEmployees()
      .subscribe((data: Company[]) => {
        this.employees = data;
    });

    this.cols = [
            { field: 'person_name', header: 'Person name' },
            { field: 'person_surname', header: 'Person Surname' },
            { field: 'actions', header: 'Actions' }
    ];
  }
}