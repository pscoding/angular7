export default class Company {
  person_name: String;
  person_surname: String;
  person_address: String;
  person_postal_number: String;
  person_city: String;
}