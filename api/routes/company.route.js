const express = require('express');
const app = express();
const companyRoutes = express.Router();

// Require Company model in our routes module
let Company = require('../models/Company');

// Defined store route
companyRoutes.route('/add').post(function (req, res) {
  let company = new Company(req.body);
  company.save()
    .then(company => {
      res.status(200).json({'company': 'employee in added successfully'});
    })
    .catch(err => {
    res.status(400).send("unable to save to database");
    });
});

// Defined get data(index or listing) route
companyRoutes.route('/').get(function (req, res) {
    Company.find(function (err, employees){
    if(err){
      console.log(err);
    }
    else {
      res.json(employees);
    }
  });
});

// Defined edit route
companyRoutes.route('/edit/:id').get(function (req, res) {
  let id = req.params.id;
  Company.findById(id, function (err, employees){
      res.json(employees);
  });
});

//  Defined update route
companyRoutes.route('/update/:id').post(function (req, res) {
    Company.findById({_id: req.params.id}, function(err, employees) {
        employees.person_name = req.body.person_name;
        employees.person_surname = req.body.person_surname;
        employees.person_address = req.body.person_address;
        employees.person_postal_number = req.body.person_postal_number;
        employees.person_city = req.body.person_city;

        employees.save().then(employees => {
          res.json('Update complete');
      })    
    });
});

// Defined delete | remove | destroy route
companyRoutes.route('/delete/:id').get(function (req, res) {
    Company.findByIdAndRemove({_id: req.params.id}, function(err, employees){
        if(err) res.json(err);
        else res.json('Successfully removed');
    });
});

module.exports = companyRoutes;