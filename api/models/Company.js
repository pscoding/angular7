const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema for Company
let Company = new Schema({
  person_name: {
    type: String
  },
  person_surname: {
    type: String
  },
  person_address: {
    type: String
  },
  person_postal_number: {
      type: Number
  },
  person_city: {
      type: String
  }
},{
    collection: 'employees'
});

module.exports = mongoose.model('Employees', Company);